//
//  main.m
//  DIExample
//
//  Created by ajjnix on 16/10/15.
//  Copyright © 2015 ajjnix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
