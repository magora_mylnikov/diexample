//
//  Created by ajjnix on 16/10/15.
//  Copyright © 2015 ajjnix. All rights reserved.
//

#import <Foundation/Foundation.h>


@class DIUser;

@interface DIUserDAO : NSObject

- (NSArray *)allUsers;

@end
