//
//  Created by ajjnix on 16/10/15.
//  Copyright © 2015 ajjnix. All rights reserved.
//

#import "DIUser.h"


@interface DIUser ()

@property (assign, nonatomic, readwrite) NSInteger userID;
@property (copy, nonatomic, readwrite) NSString *firstName;
@property (copy, nonatomic, readwrite) NSString *lastName;

@end


@implementation DIUser

- (instancetype)initWithUserID:(NSInteger)userID firstName:(NSString *)firstName lastName:(NSString *)lastName {
    self = [super init];
    if (self != nil) {
        self.userID = userID;
        self.firstName = firstName;
        self.lastName = lastName;
    }
    return self;
}

@end
