//
//  Created by ajjnix on 16/10/15.
//  Copyright © 2015 ajjnix. All rights reserved.
//

#import <Foundation/Foundation.h>


@class DIUser;
@class DIUserDAO;

@interface DIUserService : NSObject

- (instancetype)initWithDAO:(DIUserDAO *)userDAO;

- (NSArray *)allUsers;

@end
