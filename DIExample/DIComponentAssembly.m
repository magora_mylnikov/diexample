//
//  Created by ajjnix on 19/10/15.
//  Copyright © 2015 ajjnix. All rights reserved.
//

#import "DIComponentAssembly.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "DIUserService.h"
#import "DIUserDAO.h"


@implementation DIComponentAssembly

- (ViewController *)viewController {
    return [TyphoonDefinition withClass:[ViewController class] configuration:^(TyphoonDefinition *definition) {
        [definition injectProperty:@selector(userService) with:[self userService]];
    }];
}

- (DIUserService *)userService {
    return [TyphoonDefinition withClass:[DIUserService class] configuration:^(TyphoonDefinition *definition) {
        [definition useInitializer:@selector(initWithDAO:) parameters:^(TyphoonMethod *initializer) {
            DIUserDAO *userDAO = [self userDAO];
            [initializer injectParameterWith:userDAO];
        }];
        definition.scope = TyphoonScopeSingleton;
    }];
}

- (DIUserDAO *)userDAO {
    return [TyphoonDefinition withClass:[DIUserDAO class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
    }];
}

@end
