//
//  Created by ajjnix on 16/10/15.
//  Copyright © 2015 ajjnix. All rights reserved.
//

#import "ViewController.h"
#import "DIUserService.h"


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)onGetUsers:(id)sender {
    NSArray *users = [self.userService allUsers];
    NSLog(@"%@", users);
}

@end
