//
//  Created by ajjnix on 16/10/15.
//  Copyright © 2015 ajjnix. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DIUser : NSObject

@property (assign, nonatomic, readonly) NSInteger userID;
@property (copy, nonatomic, readonly) NSString *firstName;
@property (copy, nonatomic, readonly) NSString *lastName;

- (instancetype)initWithUserID:(NSInteger)userID firstName:(NSString *)firstName lastName:(NSString *)lastName;

@end
