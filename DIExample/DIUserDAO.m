//
//  Created by ajjnix on 16/10/15.
//  Copyright © 2015 ajjnix. All rights reserved.
//

#import "DIUserDAO.h"
#import "DIUser.h"


@implementation DIUserDAO

- (NSArray *)allUsers {
    DIUser *user1 = [[DIUser alloc] initWithUserID:0 firstName:@"a" lastName:@"la"];
    DIUser *user2 = [[DIUser alloc] initWithUserID:1 firstName:@"b" lastName:@"lb"];
    return @[
             user1,
             user2
             ];
}

@end
