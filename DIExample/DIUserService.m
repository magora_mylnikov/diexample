//
//  Created by ajjnix on 16/10/15.
//  Copyright © 2015 ajjnix. All rights reserved.
//

#import "DIUserService.h"
#import "DIUserDAO.h"


@interface DIUserService ()

@property (strong, nonatomic) DIUserDAO *userDAO;

@end


@implementation DIUserService

- (instancetype)initWithDAO:(DIUserDAO *)userDAO {
    self = [super init];
    if (self != nil) {
        self.userDAO = userDAO;
    }
    return self;
}

- (NSArray *)allUsers {
    return [self.userDAO allUsers];
}

@end
