//
//  Created by ajjnix on 16/10/15.
//  Copyright © 2015 ajjnix. All rights reserved.
//

#import <UIKit/UIKit.h>


@class DIUserService;

@interface ViewController : UIViewController

@property (strong, nonatomic) DIUserService *userService;

@end
